export interface StudentRequestProps {
  name: string;
  email: string;
  gender: "MALE" | "FEMALE" | "OTHER";
}

export interface StudentResponse extends StudentRequestProps {
  id: number;
}
