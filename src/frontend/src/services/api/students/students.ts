import { apiInstance } from "../api-config";
import { StudentRequestProps, StudentResponse } from "./students.types";

export const getAllStudents = async () => {
  const response = await apiInstance.get<StudentResponse[]>("students");

  return {
    data: response.data as StudentResponse[],
    status: response.status,
  };
};

export const addNewStudent = async (student: StudentRequestProps) => {
  const response = await apiInstance.post("students", JSON.stringify(student));
  return {
    data: response.data,
    status: response.status,
  };
};

export const updateStudent = async (
  studentId: number,
  student: StudentRequestProps
) => {
  const response = await apiInstance.put(
    `students/${studentId}`,
    JSON.stringify(student)
  );
  return {
    data: response.data,
    status: response.status,
  };
};

export const deleteStudent = async (studentId: number) => {
  const response = await apiInstance.delete(`students/${studentId}`);
  return {
    data: response.data,
    status: response.status,
  };
};
