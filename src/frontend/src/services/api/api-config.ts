import { create } from "apisauce";

const apiSlug = "/api/v1/";

export const apiInstance = create({
  baseURL: `${apiSlug}`,
  timeout: 10000,
  responseType: "json",
  headers: {
    "Content-Type": "application/json",
  },
});
