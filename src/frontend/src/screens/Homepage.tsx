import React, { useEffect, useState } from "react";
import "../App.css";
import { deleteStudent, getAllStudents } from "../services/api/students";
import {
  Layout,
  Menu,
  Breadcrumb,
  Table,
  Empty,
  Button,
  Tag,
  Badge,
  Avatar,
  Radio,
  Popconfirm,
  Divider,
} from "antd";
import { UserOutlined, PlusOutlined } from "@ant-design/icons";
import { StudentDrawerForm } from "../components/StudentDrawerForm";
import { Loader } from "../components/LoaderComponent";
import {
  errorNotification,
  successNotification,
} from "../services/notifications/Notification";

const { Header, Content, Footer, Sider } = Layout;

const TheAvatar = ({ name }: { name: string }) => {
  let trim = name.trim();
  if (trim.length === 0) {
    return <Avatar icon={<UserOutlined />} />;
  }
  const split = trim.split(" ");
  if (split.length === 1) {
    return <Avatar>{name.charAt(0)}</Avatar>;
  }
  return (
    <Avatar>
      {name.charAt(0)}
      {name.charAt(name.length - 1)}
    </Avatar>
  );
};

const removeStudent = async (
  studentId: number,
  callback: () => Promise<void>
) => {
  const deleteResponse = await deleteStudent(studentId);
  if (deleteResponse.status === 200) {
    successNotification(
      "Student Deleted",
      `Student with ${studentId} was deleted`
    );
    callback();
  } else {
    errorNotification(
      "There was an error",
      `${(deleteResponse.data as any).message} [${
        (deleteResponse.data as any).error
      }]`
    );
  }
};

interface Student {
  id: number;
  name: string;
  email: string;
  gender: "MALE" | "FEMALE" | "OTHER";
}

export const Homepage = () => {
  const [students, setStudents] = useState<Student[]>([]);
  const [collapsed, setCollapsed] = useState(false);
  const [fetching, setFetching] = useState(true);
  const [showDrawer, setShowDrawer] = useState(false);
  const [selectedStudent, setSelectedStudent] = useState<Student>({
    //@ts-ignore
    id: null,
    email: "",
    name: "",
    gender: "MALE",
  });

  const columns = (fetchStudents: () => Promise<void>) => [
    {
      title: "",
      dataIndex: "avatar",
      key: "avatar",
      render: (text: any, student: { name: string }) => (
        <TheAvatar name={student.name} />
      ),
    },
    {
      title: "Id",
      dataIndex: "id",
      key: "id",
    },
    {
      title: "Name",
      dataIndex: "name",
      key: "name",
    },
    {
      title: "Email",
      dataIndex: "email",
      key: "email",
    },
    {
      title: "Gender",
      dataIndex: "gender",
      key: "gender",
    },
    {
      title: "Actions",
      key: "actions",
      render: (text: any, student: any) => (
        <Radio.Group>
          <Popconfirm
            placement='topRight'
            title={`Are you sure to delete ${student.name}`}
            onConfirm={() => removeStudent(student.id, fetchStudents)}
            okText='Yes'
            cancelText='No'
          >
            <Radio.Button value='small'>Delete</Radio.Button>
          </Popconfirm>

          <Radio.Button
            onClick={() => {
              setShowDrawer(!showDrawer);
              setSelectedStudent(student);
            }}
            value='small'
          >
            Edit
          </Radio.Button>
        </Radio.Group>
      ),
    },
  ];

  const fetchStudents = async () => {
    const studentsList = await getAllStudents();
    switch (studentsList.status) {
      case 200:
        setStudents(studentsList.data as Student[]);
        setFetching(false);
        break;
      case 500:
        errorNotification(
          "There was an error",
          `${(studentsList.data as any).message} [${
            (studentsList.data as any).error
          }]`
        );
        setFetching(false);
        break;
      default:
        setFetching(false);
        break;
    }
  };

  useEffect(() => {
    fetchStudents();
  }, []);

  const renderStudents = (students: Student[]) => {
    if (fetching) {
      return <Loader />;
    }
    if (students.length <= 0) {
      return (
        <>
          <Button
            onClick={() => setShowDrawer(!showDrawer)}
            type='primary'
            shape='round'
            icon={<PlusOutlined />}
            size='small'
          >
            Add New Student
          </Button>
          <StudentDrawerForm
            showDrawer={showDrawer}
            setShowDrawer={setShowDrawer}
            fetchStudents={fetchStudents}
          />
          <Empty />
        </>
      );
    } else {
      return (
        <>
          <StudentDrawerForm
            key={selectedStudent.id}
            showDrawer={showDrawer}
            setShowDrawer={setShowDrawer}
            fetchStudents={fetchStudents}
            student={selectedStudent}
          />

          <Table
            dataSource={students}
            columns={columns(fetchStudents)}
            bordered
            title={() => (
              <>
                <Tag style={{ marginLeft: 10 }}>Number of students</Tag>
                <Badge count={students.length} className='site-badge-count-4' />
                <br />
                <br />
                <Button
                  type='primary'
                  shape='round'
                  icon={<PlusOutlined />}
                  size={"middle"}
                  onClick={() => {
                    setShowDrawer(!showDrawer);
                    setSelectedStudent({
                      //@ts-ignore
                      id: null,
                      email: "",
                      name: "",
                      gender: "MALE",
                    });
                  }}
                >
                  Add New Student
                </Button>
              </>
            )}
            pagination={{ pageSize: 50 }}
            scroll={{ y: 300 }}
            rowKey={(student) => student.id}
          />
        </>
      );
    }
  };

  return (
    <Layout style={{ minHeight: "100vh" }}>
      <Sider collapsible collapsed={collapsed} onCollapse={setCollapsed}>
        <div className='logo' />
        <Menu theme='dark' defaultSelectedKeys={["1"]} mode='inline'>
          <Menu.Item key='1' icon={<UserOutlined />}>
            Students
          </Menu.Item>
        </Menu>
      </Sider>
      <Layout className='site-layout'>
        <Header className='site-layout-background' style={{ padding: 0 }} />
        <Content style={{ margin: "0 16px" }}>
          <Breadcrumb style={{ margin: "16px 0" }}>
            <Breadcrumb.Item>Students</Breadcrumb.Item>
            <Breadcrumb.Item>List</Breadcrumb.Item>
          </Breadcrumb>
          <div
            className='site-layout-background'
            style={{ padding: 24, minHeight: 360 }}
          >
            {renderStudents(students)}
          </div>
        </Content>
        <Footer style={{ textAlign: "center" }}>
          By Rishabh
          <Divider>
            <a
              target={"_blank"}
              href='https://www.google.com/'
              rel='noreferrer'
            >
              Click here to access
            </a>
          </Divider>
        </Footer>
      </Layout>
    </Layout>
  );
};
