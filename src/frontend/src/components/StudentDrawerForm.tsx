import { Drawer, Input, Col, Select, Form, Row, Button } from "antd";
import { FC, useState } from "react";
import { Loader } from "./LoaderComponent";
import {
  errorNotification,
  successNotification,
} from "../services/notifications/Notification";
import { addNewStudent, updateStudent } from "../services/api/students";
import {
  StudentRequestProps,
  StudentResponse,
} from "../services/api/students/students.types";

const { Option } = Select;

interface StudentDrawerFormProps {
  showDrawer: boolean;
  setShowDrawer: (value: boolean) => void;
  fetchStudents?: () => void;
  student?: StudentResponse;
}

export const StudentDrawerForm: FC<StudentDrawerFormProps> = ({
  showDrawer,
  setShowDrawer,
  fetchStudents,
  student,
}) => {
  const [isLoading, setIsLoading] = useState(false);

  const editing = (student?.email.length || 0) > 0;

  const onCLose = () => setShowDrawer(false);

  const callAddNewStudent = async (values: StudentRequestProps) => {
    const student = await addNewStudent(values);
    if (student.status === 200) {
      setIsLoading(false);
      onCLose();
      successNotification(
        "Student successfully added",
        `${values.name} was added to system`
      );
      fetchStudents?.();
    } else {
      errorNotification(
        "There was an issue",
        `${(student.data as any).message} [${(student.data as any).error}]`,
        "bottomLeft"
      );
      setIsLoading(false);
    }
  };

  const callEditStudent = async (values: any) => {
    const updateResponse = await updateStudent((student as any).id, values);
    if (updateResponse.status === 200) {
      setIsLoading(false);
      onCLose();
      successNotification(
        "Student Updated",
        `Student with ${student?.id} was updated`
      );
      fetchStudents?.();
    } else {
      errorNotification(
        "There was an error",
        `${(updateResponse.data as any).message} [${
          (updateResponse.data as any).error
        }]`
      );
      setIsLoading(false);
    }
  };

  const closeDrawerWithoutEditing = () => {
    setIsLoading(false);
    onCLose();
  };

  const onFinish = (studentData: StudentRequestProps) => {
    const isEdited =
      student?.email !== studentData.email ||
      student.name !== studentData.name ||
      student.gender !== studentData.gender;
    setIsLoading(true);
    editing
      ? isEdited
        ? callEditStudent(studentData)
        : closeDrawerWithoutEditing()
      : callAddNewStudent(studentData);
  };

  const onFinishFailed = (errorInfo: any) => {
    alert(JSON.stringify(errorInfo, null, 2));
  };

  return (
    <Drawer
      title={editing ? "Update Student" : "Create new student"}
      width={720}
      onClose={onCLose}
      visible={showDrawer}
      bodyStyle={{ paddingBottom: 80 }}
      footer={
        <div
          style={{
            textAlign: "right",
          }}
        >
          <Button onClick={onCLose} style={{ marginRight: 8 }}>
            Cancel
          </Button>
        </div>
      }
    >
      <Form
        layout='vertical'
        onFinishFailed={onFinishFailed}
        onFinish={onFinish}
        hideRequiredMark
      >
        <Row gutter={16}>
          <Col span={12}>
            <Form.Item
              name='name'
              label='Name'
              initialValue={student?.name}
              rules={[{ required: true, message: "Please enter student name" }]}
            >
              <Input placeholder='Please enter student name' />
            </Form.Item>
          </Col>
          <Col span={12}>
            <Form.Item
              name='email'
              label='Email'
              initialValue={student?.email}
              rules={[
                { required: true, message: "Please enter student email" },
              ]}
            >
              <Input placeholder='Please enter student email' />
            </Form.Item>
          </Col>
        </Row>
        <Row gutter={16}>
          <Col span={12}>
            <Form.Item
              name='gender'
              label='gender'
              initialValue={student?.gender}
              rules={[{ required: true, message: "Please select a gender" }]}
            >
              <Select placeholder='Please select a gender'>
                <Option value='MALE'>MALE</Option>
                <Option value='FEMALE'>FEMALE</Option>
                <Option value='OTHER'>OTHER</Option>
              </Select>
            </Form.Item>
          </Col>
        </Row>
        <Row>
          <Col span={12}>
            <Form.Item>
              <Button type='primary' htmlType='submit'>
                Submit
              </Button>
            </Form.Item>
          </Col>
        </Row>
        <Row>{isLoading && <Loader />}</Row>
      </Form>
    </Drawer>
  );
};
