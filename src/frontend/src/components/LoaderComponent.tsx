import { LoadingOutlined } from "@ant-design/icons";
import { Spin } from "antd";

const LoaderIcon = <LoadingOutlined style={{ fontSize: 24 }} />;

export const Loader = () => {
  return <Spin indicator={LoaderIcon} />;
};
